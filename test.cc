#include <time.h>
#include <vector>
#include <hip/hip_runtime.h>
#include <rocblas.h>

uint64_t get_time_in_nanosec()
{
    struct timespec time;
    clock_gettime(CLOCK_MONOTONIC, &time);
    uint64_t timeInSec = time.tv_sec * 1e9 + time.tv_nsec;
    return timeInSec;
}

int main()
{
    int size = 64;
    int repeat = 10;
    float alpha = 1.0;
    float beta = 0.0;
    std::vector<float> hA, hB, hC;
    float *dA, *dB, *dC;
    rocblas_handle handle;
    rocblas_status status;
    hipEvent_t start, stop;

    hipEventCreate(&start);
    hipEventCreate(&stop);

    status = rocblas_create_handle(&handle);
    if (status != rocblas_status_success)
    {
        printf("Failed to create handle.\n");
        exit(2);
    }

    rocblas_initialize();

    while (size <= 8192)
    {
        for (int r = 0; r < repeat; r++)
        {
            hA.resize(size * size);
            hB.resize(size * size);

            for (int i = 0; i < size * size; i++)
            {
                hA[i] = ((float)rand() / (float)RAND_MAX);
                hB[i] = ((float)rand() / (float)RAND_MAX);
            }

            hipMalloc(&dA, size * size * sizeof(float));
            hipMalloc(&dB, size * size * sizeof(float));
            hipMalloc(&dC, size * size * sizeof(float));

            rocblas_set_matrix(size, size, sizeof(float), hA.data(), size, dA, size);
            rocblas_set_matrix(size, size, sizeof(float), hB.data(), size, dB, size);

            hipEventRecord(start);
            status = rocblas_sgemm(handle,
                                   rocblas_operation_none, rocblas_operation_transpose,
                                   size, size, size,
                                   &alpha, dA, size,
                                   dB, size,
                                   &beta,
                                   dC, size);
            hipEventRecord(stop);

            if (status != rocblas_status_success)
            {
                printf("Failed to run rocblas.\n");
                exit(2);
            }

            hipEventSynchronize(stop);

            float milliseconds = 0;
            hipEventElapsedTime(&milliseconds, start, stop);

            printf("%d, %f\n", size, milliseconds);

            hipFree(dA);
            hipFree(dB);
            hipFree(dC);
        }
        size += 64;
    }

    return 0;
}